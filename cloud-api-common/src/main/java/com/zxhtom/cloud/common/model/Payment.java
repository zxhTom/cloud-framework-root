package com.zxhtom.cloud.common.model;

import lombok.Data;

/**
 * @author 张新华
 * @version V1.0
 * @Package com.zxhtom.cloud.payment.model
 * @date 2021年03月16日, 0016 10:03
 */
@Data
public class Payment {
    private Long id;
    private String serial;
}

package com.zxhtom.cloud.common.model;

import lombok.Data;

/**
 * @author 张新华
 * @version V1.0
 * @Package com.zxhtom.cloud.payment.model
 * @date 2021年03月16日, 0016 10:00
 */
@Data
public class ResultInfo {
    private Integer code;
    private String msg;
    private Object data;
}

package com.zxhtom.cloud.payment.service;

import com.zxhtom.cloud.common.model.Payment;
import com.zxhtom.cloud.common.model.ResultInfo;

/**
 * @author 张新华
 * @version V1.0
 * @Package com.zxhtom.cloud.payment.service
 * @date 2021年03月16日, 0016 09:59
 */
public interface PaymentService {
    /**
    * @author zxhtom
    * @Description 添加支付类
    * @Date 10:03 2021年03月16日, 0016
    * @Param 
    * @return com.zxhtom.cloud.payment.model.ResultInfo
    */
    ResultInfo create(Payment payment);

    ResultInfo getByPrimary(Long id);
}

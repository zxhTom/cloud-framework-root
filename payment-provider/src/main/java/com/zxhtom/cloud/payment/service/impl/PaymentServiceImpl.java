package com.zxhtom.cloud.payment.service.impl;

import com.zxhtom.cloud.common.model.Payment;
import com.zxhtom.cloud.common.model.ResultInfo;
import com.zxhtom.cloud.payment.mapper.PaymentMapper;
import com.zxhtom.cloud.payment.service.PaymentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author 张新华
 * @version V1.0
 * @Package com.zxhtom.cloud.payment.service.impl
 * @date 2021年03月16日, 0016 09:59
 */
@Service
public class PaymentServiceImpl implements PaymentService {
    @Autowired
    PaymentMapper paymentMapper;
    @Override
    public ResultInfo create(Payment payment) {
        ResultInfo resultInfo = new ResultInfo();
        Integer insert= 0 ;
        try {
            insert = paymentMapper.insert(payment);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (insert > 0) {
            resultInfo.setCode(200);
            resultInfo.setMsg("success");
            resultInfo.setData(insert);
        } else {
            resultInfo.setData(null);
            resultInfo.setMsg("fail");
            resultInfo.setCode(400);
        }
        return resultInfo;
    }

    @Override
    public ResultInfo getByPrimary(Long id) {
        Payment payment = paymentMapper.select(id);
        ResultInfo resultInfo = new ResultInfo();
        resultInfo.setCode(200);
        resultInfo.setMsg("success");
        resultInfo.setData(payment);
        return resultInfo;
    }
}

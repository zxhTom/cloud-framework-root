package com.zxhtom.cloud.payment.mapper;

import com.zxhtom.cloud.common.model.Payment;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author 张新华
 * @version V1.0
 * @Package com.zxhtom.cloud.payment.mapper
 * @date 2021年03月16日, 0016 09:59
 */
@Mapper
public interface PaymentMapper {

    Integer insert(Payment payment);

    Payment select(Long id);
}

package com.zxhtom.cloud.payment.controller;

import com.zxhtom.cloud.common.model.Payment;
import com.zxhtom.cloud.common.model.ResultInfo;
import com.zxhtom.cloud.payment.service.PaymentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @author 张新华
 * @version V1.0
 * @Package com.zxhtom.cloud.payment.controller
 * @date 2021年03月16日, 0016 09:58
 */
@RestController
@RequestMapping(value = "/payment")
public class PaymentController {
    @Autowired
    PaymentService paymentService;

    @RequestMapping(value = "/create" , method = RequestMethod.POST)
    public ResultInfo create(@RequestBody Payment payment){
        return paymentService.create(payment);
    }


    @RequestMapping(value = "/get/{id}" , method = RequestMethod.GET)
    public ResultInfo get(@PathVariable("id") Long id){
        return paymentService.getByPrimary(id);
    }
}

package com.zxhtom.cloud.order.controller;

import com.zxhtom.cloud.common.model.ResultInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.resource.ResourceUrlProviderExposingInterceptor;

/**
 * @author 张新华
 * @version V1.0
 * @Package com.zxhtom.cloud.order
 * @date 2021年03月16日, 0016 10:25
 */
@RestController
@RequestMapping(value = "/order")
public class OrderController {
    private final static String PAYMENT_URL = "http://localhost:8001/payment";
    @Autowired
    RestTemplate restTemplate;

    @RequestMapping(value = "/getpayment/{id}")
    public ResultInfo getPaymentInfo(@PathVariable("id") Long id) {
        return restTemplate.getForObject(PAYMENT_URL+"/get/"+id, ResultInfo.class);
    }
}

package com.zxhtom.cloud.order.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

/**
 * @author 张新华
 * @version V1.0
 * @Package com.zxhtom.cloud.order.config
 * @date 2021年03月16日, 0016 10:33
 */
@Configuration
public class RestConfig {
    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }
}
